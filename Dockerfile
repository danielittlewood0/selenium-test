# Dockerfile

FROM ruby:3.0

RUN apt-get update
RUN apt-get install -yy less vim # helps with pry
COPY . /code
WORKDIR /code
RUN bundle install

