require 'selenium-webdriver'

# this is a hack to ensure firefox is started before the test runs
sleep 10

driver = Selenium::WebDriver.for :remote, url: "http://firefox:4444"
driver.get "http://www.google.com"
driver.close
